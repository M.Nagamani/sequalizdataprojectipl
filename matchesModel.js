const Sequelize = require('sequelize');
//const sequelize =require('./serverConnection.js').serverConnection ;
function iplMatch(sequelize){
 // console.log(sequelize);
const iplMatches =sequelize.define('iplmatch', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  season: Sequelize.INTEGER,
  city: Sequelize.STRING,
  date: Sequelize.DATEONLY,
  team1: Sequelize.STRING,
  team2: Sequelize.STRING,
  toss_winner: Sequelize.STRING,
  toss_decision: Sequelize.STRING,
  result: Sequelize.STRING,
  dl_applied: Sequelize.INTEGER,
  winner: Sequelize.STRING,
  win_by_runs: Sequelize.INTEGER,
  win_by_wickets: Sequelize.INTEGER,
  player_of_match: Sequelize.STRING,
  venue: Sequelize.STRING,
  umpire1: Sequelize.STRING,
  umpire2: Sequelize.STRING,
  umpire3: Sequelize.STRING
}, {});
return iplMatches;
}
module.exports={
  iplMatch:iplMatch
}
