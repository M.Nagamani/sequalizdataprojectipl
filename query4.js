const Sequelize = require('sequelize');
const serverConnections =require('./serverConnection.js').serverConnection ;
const iplMatch=require('./matchesModel.js').iplMatch;
const ipldeliveries=require('./deliveriesModel').ipldeliveries;
iplMatches=iplMatch(serverConnections())
iplDeliveries=ipldeliveries(serverConnections())
iplDeliveries.findAll({
  raw: true,
  include: [{
    model: iplMatches,
    where: {
      season: '2015'
    },
    require: true,
    attributes: []
  }],
  attributes: ['bowler', [Sequelize.fn('sum', iplDeliveries.sequelize.col('total_runs')), 'totalRuns'],
    [Sequelize.fn('sum', iplDeliveries.sequelize.col('legbye_runs')), 'legByeRuns'],
    [Sequelize.fn('sum', iplDeliveries.sequelize.col('bye_runs')), 'byeRuns'],
    [Sequelize.fn('count', iplDeliveries.sequelize.col('total_runs')), 'balls']
  ],
  group: ['bowler']
}).then((result) => {
  let economyRate = [];
  let bowler = [];
  let calculation = [];
  let runsConcededPerBowler = [];
  for (let key in result) {
    bowler[key] = result[key]['bowler'];
    runsConcededPerBowler[key] = result[key]['totalRuns'] - result[key]['legByeRuns'] - result[key]['byeRuns'];
    calculation[key] = runsConcededPerBowler[key] * 6 / (result[key]['balls']);
    economyRate.push([bowler[key], calculation[key]])
  }
  economyRate.sort((a, b) => { return a[1] - b[1]; });
  console.log(economyRate.slice(0, 10));
})