const Sequelize = require('sequelize');
const serverConnections =require('./serverConnection.js').serverConnection ;
const iplMatch=require('./matchesModel.js').iplMatch;
const ipldeliveries=require('./deliveriesModel').ipldeliveries;
iplMatches=iplMatch(serverConnections())
iplDeliveries=ipldeliveries(serverConnections())
iplDeliveries.findAll({
  raw:true,
  include:[{
    model:iplMatches,
    where:{
      season:'2016'
    },
    require:true,
    attributes:[]
  }],
  attributes:['bowling_team',[Sequelize.fn('sum',iplDeliveries.sequelize.col('extra_runs')),'extraRunsCount']
],
group:['bowling_team']
}).then((result)=>{
  console.log(result);
})