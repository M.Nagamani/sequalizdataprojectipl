const Sequelize = require('sequelize');
const iplMatch=require('./matchesModel.js').iplMatch;
function ipldeliveries(sequelize){
const iplDeliveries = sequelize.define('ipldeliverie', {
  match_id: {
    type: Sequelize.INTEGER,
    // foreignKey: 'iplMatches'
    references: {
      model: iplMatches,
      key: 'id'
    }
  },
  inning: Sequelize.INTEGER,
  batting_team: Sequelize.STRING,
  bowling_team: Sequelize.STRING,
  over: Sequelize.INTEGER,
  ball: Sequelize.INTEGER,
  batsman: Sequelize.STRING,
  non_striker: Sequelize.STRING,
  bowler: Sequelize.STRING,
  is_super_over: Sequelize.INTEGER,
  wide_runs: Sequelize.INTEGER,
  bye_runs: Sequelize.INTEGER,
  legbye_runs: Sequelize.INTEGER,
  noball_runs: Sequelize.INTEGER,
  penalty_runs: Sequelize.INTEGER,
  batsman_runs: Sequelize.INTEGER,
  extra_runs: Sequelize.INTEGER,
  total_runs: Sequelize.INTEGER,
  player_dismissed: Sequelize.STRING,
  dismissal_kind: Sequelize.STRING,
  fielder: Sequelize.STRING
}, {})
iplDeliveries.belongsTo(iplMatches, {
    targetKey: 'id',       ///to join the tables
    foreignKey: 'match_id'
  });
return iplDeliveries;
}
module.exports={
    ipldeliveries:ipldeliveries
  }
  
